syms x1 x2 x3 x4 x5 x6 x7 x8 u x
m11 = 2*wheel.Iyy+2*(gbox.N)^2*mot.rot.Iyy+(body.m+2*wheel.m+2*mot.rot.m)*(wheel.r)^2; 
m12 = 2*gbox.N*(1-gbox.N)*mot.rot.Iyy; 
m22 = body.Iyy + 2*(1-gbox.N)^2*mot.rot.Iyy+body.m*(body.zb)^2+2*mot.rot.m*(mot.rot.zb)^2;
km12 = (body.m*body.zb+2*mot.rot.m*mot.rot.zb)*wheel.r;
c11 = 0; 
c12 = -(body.m*body.zb+2*mot.rot.m*mot.rot.zb)*wheel.r*sin(x2)*x4; 
c21 = 0; 
c22 = 0;
f11 = 2*(gbox.B+wheel.B); 
f12 = -2*gbox.B;
f21 = -2*gbox.B;
f22 = 2*gbox.B;
g1 = 0; 
g2 = -(body.m*body.zb+2*mot.rot.m*mot.rot.zb)*g;
M = [m11 , m12+km12*cos(x2) ; m12+km12*cos(x2) , m22];
C = [c12 , c12 ; c21 , c22];
F = [f11 , f12 ; f21 , f22];
G = [ g1 ; g2*sin(x2)];

[x] = inv(M)*( [1;-1]*u-F*[x3;x4]-C*[x3;x4]-G);