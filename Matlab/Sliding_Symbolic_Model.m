load_params;

syms m11 m121 m122 m22 X1 X2 X3 X4 N1 N2 N3 N4 XI DX4 DX3 F3 F4 G3 G4 U c12 f11 f12 f21 f22 G21

T = [X1 ; (m22+m121+m122*cos(X2))*X4+(m11+m121+m122*cos(X2))*X3 ; X2 ; X4]; 


Tdot = [X3 ; (m22+m121+m122*cos(X2))*DX4-m122*sin(X2)*X4*X4+(m11+m121+m122*cos(X2))*DX3-m122*sin(X2)*X3*X4 ; X4 ; DX4];

m = [m11 , m121+m122*cos(X2) ; m121+m122*cos(X2) , m22];
c = [ 0  ,  c12*sin(X2)*X4  ; 0 , 0];
f = [f11 , f12 ; f21 , f22];
gg = [ 0 ; G21*sin(X2)];

C1 = N1;
C2 = N3;
C3 = (N2-(m22+m121+m122*cos(N3))*XI)/(m11+m121+m122*cos(N3));
C4 = XI;
C10 = inv(m)*(U*[1;-1]-c*[X3;X4]-f*[X3;X4]-gg);
C5 = C10(1); C5 = subs(C5,[X1,X2,X3,X4],[C1,C2,C3,C4]);
C6 = C10(2); C6 = subs(C6,[X1,X2,X3,X4],[C1,C2,C3,C4]);

Tdot2 = subs(Tdot,[X1,X2,X3,X4,DX3,DX4],[C1,C2,C3,C4,C5,C6]);

fa = Tdot2(1:3);

parN = [diff(fa,N1),diff(fa,N2),diff(fa,N3)];
A = subs(parN,[N1,N2,N3,XI],[0,0,0,0]);

parXI = diff(fa,XI);
B = subs(parXI,[N1,N2,N3,XI],[0,0,0,0]);

Al = subs(A,[m11,m121,m122,m22,c12,f11,f12,f21,f22,G21],[M11,M12_1,M12_2,M22,C12,Fv11,Fv12,Fv21,Fv22,g12]);
Bl = subs(B,[m11,m121,m122,m22,c12,f11,f12,f21,f22,G21],[M11,M12_1,M12_2,M22,C12,Fv11,Fv12,Fv21,Fv22,g12]);


Co = ctrb(Al,Bl);

% p = [-5 -7 -1.5];

A = [ 0,  36028797018963968/133763904824565,                                 0;
 0, -4503599627370496/5573496034356875, 4201547703962385/9007199254740992;
 0,                                  0,                                 0];

B = [
   -800733698739029/535055619298260;
 800733698739029/178351873099420000;
                                  1];
                              
 p = [-5 -7 -1.5];                              
K = place(A,B,p);




















