
clearvars

%% load paramas
balrob_params;

%Inertia Matrix Definition
M11 = 2*wheel.Iyy + 2*gbox.N^2*mot.rot.Iyy + (body.m + 2*wheel.m + 2*mot.rot.m)*wheel.r^2;
M12_1 = 2*gbox.N*(1-gbox.N)*mot.rot.Iyy;
M12_2 = (body.m*body.zb + 2*mot.rot.m*mot.rot.zb)*wheel.r; %*cos(theta)
M12 =  M12_1 + M12_2;
M21 = M12;
M22 = body.Iyy + 2*(1-gbox.N)^2*mot.rot.Iyy + body.m*body.zb^2 + 2*mot.rot.m*mot.rot.zb^2;

M=[M11 M12;M21 M22];

%Centrifugal Matrix
C11 =0; C21 =0; C22 = 0;
C12 = -(body.m*body.zb + 2*mot.rot.m*mot.rot.zb)*wheel.r; %*sin(theta)*theta
C= [C11 C12;C21 C22];

%Matrix of viscous friction coefficients
Fv11 = 2*(gbox.B + wheel.B);
Fv12 = -2*gbox.B; Fv21 = -2*gbox.B;
Fv22 = 2*gbox.B;

Fv = [Fv11 Fv12; Fv21 Fv22];

%torque contribution due to gravity
g11 = 0;
g12 = -(body.m*body.zb + 2*mot.rot.m*mot.rot.zb)*g; %*sin(theta)
G = [g11 g12];

%% Simple state observer

% Complementary filters pair
fc=0.35; %cutoff frequency
Tc = 1/(2*pi*fc); 

% Speed filters coefficients
N = 3; %weighted delay (notch) 

%% Controller design

% Friction re-definition
UnoDiNoi = [1 -1;-1 1];
Fv1 = Fv + (2*gbox.N^2*mot.Kt*mot.Ke/mot.R)*UnoDiNoi;   %Fv'

Kp1 = 100;
Kp2 = 100;
K = diag([Kp1 Kp2]);
Kp2 = 1;

delta = .0001;
W = 1;

%% Simulation parameters

gam_star = 0;
load_dist = 10/drv.duty2V;

% robot initial condition
x0 =[0,5*deg2rad,0,0]';  %apply with either reference or disturbance or both



load2mot = gbox.N;
mot2load = 1/load2mot;


%discretization
      Tc = 1/(2*pi*0.35);
      s = tf('s');
      H = (Tc*3*s^2+Tc*s*3+1)/(Tc*s+1)^3;
      z = tf('z');
      HEA = (Tc^2*3*((1-1/z)/Ts)^2+Tc*3*(1-1/z)/Ts+1)/(Tc*(1-1/z)/Ts+1)^3;
      HEA = c2d(H,Ts,'tustin');
      [num,den]=tfdata(HEA,'v');
      

lambda = M12_2/M22*cos(pi/2-pi/50);
ct = 2*sqrt(lambda*9.81);
cg = ct/50;




A = [ 0,  36028797018963968/133763904824565,                                 0;
 0, -4503599627370496/5573496034356875, 4201547703962385/9007199254740992;
 0,                                  0,                                 0];

B = [
   -800733698739029/535055619298260;
 800733698739029/178351873099420000;
                                  1];
                              
 p = [-5 -7 -1.5];                              
K = place(A,B,p);

