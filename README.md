# Nonlinear Control for Segway

Nonlinear control design for Segway-like robot. The task is to balance the unstable upward equilibrium point of the balancing robot, via a nonlinear control. 
Sliding mode with I/O feedback linearization was used for the design.

To run the project:

1. run the MATLAB script "roboData";
2. run the Simulink model "final_sim".

The Simulink model "final_test" requires connection to a real balancing robot.